import { LitElement, html } from 'lit-element';

class PersonaMainDm extends LitElement {

  static get properties() {
    return {
      people: {type: Array}
    };
  }

  constructor() {
      super();

      this.people = [
        {
          name: "Bruce Wayne",
          yearsInCompany: 5,
          profile:"Bruce Wayne, a billionaire orphan? We used to make up stories about you man, legends.",
          photo: {
            src: "./img/persona.jpg",
            alt: "Bruce Wayne"
          }
        }, {
          name: "Jocker",
          yearsInCompany: 3,
          profile: "We used to make up stories",
          photo: {
            src: "./img/persona.jpg",
            alt: "Jocker"
          }
        }, {
          name: "Alfred",
          yearsInCompany: 8,
          profile: "And like shipwrecked men turning to seawater",
          photo: {
            src: "./img/persona.jpg",
            alt: "Alfred"
          }
        }, {
          name: "Bane",
          yearsInCompany: 1,
          profile: "a billionaire orphan? We used to make up stories about you man",
          photo: {
            src: "./img/persona.jpg",
            alt: "Bane"
          }
        }, {
          name: "CatWoman",
          yearsInCompany: 2,
          profile: "We used to make up stories about you man",
          photo: {
            src: "./img/persona.jpg",
            alt: "CatWoman"
          }
        }

      ];

  }

  updated(changedProperties) {
    console.log("updated");

    if (changedProperties.has("people")) {
      console.log("Ha cambiado la propiedad people en persona-main-dm");

      this.dispatchEvent(new CustomEvent("people-data-updated", {
        detail: {
          people: this.people
        }
      }))
    }



  }
}






customElements.define('persona-main-dm', PersonaMainDm)
