import { LitElement, html } from 'lit-element';

class TestApi extends LitElement {

  static get properties() {
    return {
      movies: {type: Array}
    };
  }

  constructor() {
    super();

    this.movies = [];
    this.getMovieData();
  }

  render() {
      return html`
        ${this.movies.map(
          movie => html`<div>La película ${movie.title} fué dirigida por ${movie.director}</div>`
        )}

      `;
  }
  getMovieData() {
    console.log("getMovieData");
    console.log("Obteniendo datos de las películas");

    let xhr = new XMLHttpRequest();

    xhr.onload = () => {
      if(xhr.status === 200) {
        console.log("petición completada correctamente");

        let APIResponse = JSON.parse(xhr.responseText);
        console.log(APIResponse);

        this.movies = APIResponse.results;

      }

    }
    xhr.open("GET", "https://swapi.dev/api/films/");
    xhr.send();
    console.log("Fin de getMovieData");

  }

}

customElements.define('test-api', TestApi)
