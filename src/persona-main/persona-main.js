import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../persona-main-dm/persona-main-dm.js';

class PersonaMain extends LitElement {


  static get properties() {
    return {
      people: {type: Array},
      showPersonForm: {type: Boolean},
      maxYearsInCompanyFilter: {type: Number}
    };
  }

  constructor() {
      super();

      this.people = [];
      this.showPersonForm = false;
  }


  render() {
      return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <h2 class="text-center">Personas</h2>
          <div class="row" id="peopleList">
              <div class="row row-cols-1 row-cols-sm-4">
                  ${this.people.filter(
                    person => person.yearsInCompany <= this.maxYearsInCompanyFilter
                  ).map(
                    person => html`<persona-ficha-listado
                      fname="${person.name}"
                      yearsInCompany="${person.yearsInCompany}"
                      profile="${person.profile}"
                      .photo="${person.photo}"
                      @delete-person="${this.deletePerson}"
                      @info-person="${this.infoPerson}"></persona-ficha-listado>`
                  )}
              </div>
          </div>

          <div class="row">
              <persona-form id="personForm" class="d-none border rounded border-primary"
              @persona-form-close="${this.personFormClose}"
              @persona-form-store="${this.personFormStore}"
              ></persona-form>

          </div>
          <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>

      `;
  }

  updated(changedProperties) {
    console.log("updated");

    if(changedProperties.has("showPersonForm")) {
      console.log("ha cambiado el valor de showPersonForm en persona-main");

      if(this.showPersonForm) {
        this.showPersonFormData();
      }else {
        this.showPersonList();
      }
    }
      if (changedProperties.has("people")) {
        console.log("ha cambiado el valor de la propiedad people en persona-main");

        this.dispatchEvent(new CustomEvent("updated-people", {
          detail: {
            people: this.people
          }
        })
        );
      }

      if (changedProperties.has("maxYearsInCompanyFilter")) {
        console.log("Ha cambiado el valor de la propiedad maxYearsInCompanyFilter en persona-main");
        console.log("Se van a mostrar las personas cuya antigüedad máxima sea " + this.maxYearsInCompanyFilter + "años");
      }

  }
  peopleDataUpdated(event){
    this.people = event.detail.people;
}


  personFormClose() {
    console.log("personFormClose");
    console.log("Se ha cerrado el formulario de la persona");

    this.showPersonForm = false;
  }

  personFormStore(e) {
    console.log("personFormStore");
    console.log("Se va a almacenaruna persona");

    console.log("Name de la persona es " + e.detail.person.name);
    console.log("profile de la persona es " + e.detail.person.profile);
    console.log("yearsInCompany de la persona es " + e.detail.person.yearsInCompany);
    console.log(e.detail);

    if(e.detail.editingPerson === true) {
      console.log("se va a actualizar la persona de nombre " + e.detail.person.name);

      this.people = this.people.map(
        person => person.name === e.detail.person.name
        ? person = e.detail.person : person);


    } else {
      console.log("se va a guardar una persona nueva");
      this.people = [...this.people, e.detail.person];
    }
    //   this.people.push(e.detail.person);
    //

    console.log("Fin proceso guardado");
    this.showPersonForm = false;
  }

  showPersonList() {
    console.log("showPersonList");
    console.log("mostrando el istado de personas");
    this.shadowRoot.getElementById("personForm").classList.add("d-none");
    this.shadowRoot.getElementById('peopleList').classList.remove('d-none');
  }

  showPersonFormData() {
    console.log("showPersonFormData");
    console.log("mostramos el formulario de personas");

    this.shadowRoot.getElementById('personForm').classList.remove('d-none');
    this.shadowRoot.getElementById('peopleList').classList.add('d-none');
  }
  deletePerson(e) {
    console.log("deletePerson en persona-main");
    console.log("se va a borrar la persona de nombre " + e.detail.name);

    this.people = this.people.filter(
      person => person.name != e.detail.name
    );
  }

  infoPerson(e) {
    console.log("info-person");
    console.log("se ha pedido más info de la persona " + e.detail.name);

    let chosenPerson = this.people.filter(
      person => person.name === e.detail.name
    );

    let person = {};
    person.name = chosenPerson[0].name;
    person.profile = chosenPerson[0].profile;
    person.yearsInCompany = chosenPerson[0].yearsInCompany;

    this.shadowRoot.getElementById("personForm").person = person;
    this.shadowRoot.getElementById("personForm").editingPerson = true;
    this.showPersonForm = true;

  }

}

customElements.define('persona-main', PersonaMain)
